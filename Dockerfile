FROM rootproject/root-conda:6.18.04

COPY . /home/burak_analysis

WORKDIR /home/burak_analysis

RUN echo ">>> Compile skimming executable ..." && \
    COMPILER=$(root-config --cxx) && \
    FLAGS=$(root-config --cflags --libs) && \
    $COMPILER -g -std=c++11 -O3 -Wall -Wextra -Wpedantic -o skim skim.cxx $FLAGS

